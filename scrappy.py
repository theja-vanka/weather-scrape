from bs4 import BeautifulSoup
from selenium import webdriver
import pandas as pd
import threading
import logging

logging.basicConfig(filename="scrape.log", 
                    format='%(asctime)s %(message)s', 
                    filemode='w',
                    level=logging.ERROR)

def split_three(dataframe):
    temp = [[],[],[]]
    for i,j in enumerate(dataframe):
        if i%3 == 0:
            temp[i%3].append(j)
        elif i%3 == 1:
            temp[i%3].append(j)
        else :
            temp[i%3].append(j)
    return temp

def divide_chunks(l, n): 
    # looping till length l 
    for i in range(0, len(l), n):  
        yield l[i:i + n]

def program_main(locations,years,months):
    for location in locations:
        for year in years:
            for mon in months:
                URL = "https://www.wunderground.com/history/monthly/in/"+str(location)+"/"+str(lockey[location])+"/date/"+str(year)+"-"+str(mon)+""
                driver = webdriver.Firefox()
                driver.get(URL)
                html = driver.page_source
                soup = BeautifulSoup(html,"html.parser")
                driver.close()
                try:
                    table = soup.find_all("table",{"class": "days"})
                    temp = table[0].find_all('table')
                    loop0 = temp[0].find_all('td')
                    loop1 = temp[1].find_all('td')
                    loop2 = temp[2].find_all('td')
                    loop3 = temp[3].find_all('td')
                    loop4 = temp[4].find_all('td')
                    loop5 = temp[5].find_all('td')
                    loop6 = temp[6].find_all('td')
                    looplst = [loop0,loop1,loop2,loop3,loop4,loop5,loop6]
                    final_data = [[],[],[],[],[],[],[]]

                    iterator = 0
                    for i in looplst:
                        for j in i:
                            final_data[iterator].append(j.text.strip())
                        iterator = iterator + 1
                    date = final_data[0][1:]
                    temperature = split_three(final_data[1][3:])    
                    dew = split_three(final_data[2][3:])
                    humidity = split_three(final_data[3][3:])
                    wind = split_three(final_data[4][3:])
                    pressure = split_three(final_data[5][3:])

                    if len(final_data[6]) < 45:
                        precipation = [final_data[6][1:]]
                        flag = 0
                    else :    
                        precipation = split_three(final_data[6][3:])
                        flag = 1

                    final_dataframe = pd.DataFrame(list(zip(date,
                                                            temperature[0], temperature[1], temperature[2],
                                                            dew[0], dew[1], dew[2],
                                                            humidity[0],humidity[2],
                                                            wind[0],wind[2],
                                                            pressure[0],pressure[2],
                                                            precipation[flag]
                                                        )), 
                                columns =['Day',
                                            'Temperature(Max)', 'Temperature(Avg)', 'Temperature(Min)',
                                            'DewPoint(Max)', 'DewPoint(Avg)', 'DewPoint(Min)',
                                            'Humidity(Max)','Humidity(Min)',
                                            'WindSpeed(Max)', 'WindSpeed(Min)',
                                            'Pressure(Max)', 'Pressure(Min)',
                                            'Precipation(Avg)'
                                            ])
                    final_dataframe.to_csv('csv_frames/frame_'+str(location)+'_'+str(year)+'_'+str(mon)+'.csv',index=False)
                    logging.info('Completed: '+str(location)+"_"+str(year)+"_"+str(mon))
                except:
                    logging.error('Did not work for: '+str(location)+"_"+str(year)+"_"+str(mon))
                    print('Did not work for: '+str(location)+"_"+str(year)+"_"+str(mon))

years = [2017,2018,2019]
months = [x for x in range(1,13,1)]
lockey = {}
places = []
threads = []

location_df = pd.read_csv('all_airports_codes.csv')

for i in location_df.values:
    lockey[i[1]] = i[0]
    places.append(i[1])

n =  int(len(places)/5)
newlocation = list(divide_chunks(places, n))

for i in range(len(newlocation)):
    threads.append(threading.Thread(target=program_main, args=(newlocation[i],years,months)))

for i in threads:
    i.daemon = True
    i.start()

for i in threads:
    i.join()

print("Done!")
